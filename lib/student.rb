class Student
  attr_accessor :first_name, :last_name, :courses

  def initialize(first_name, last_name)
    @first_name = first_name
    @last_name = last_name
    @courses = []
  end

  def name
    "#{@first_name} #{@last_name}"
  end

  def enroll(course)
    raise 'WRONG' if courses.any? { |cour| cour.conflicts_with?(course) }
    unless @courses.include?(course) && course.students.include?(self)
      @courses << course
      course.students << self
    end
  end

  def course_load
    hash = Hash.new(0)
    @courses.each { |course| hash[course.department] += course.credits }
    hash
  end
end
